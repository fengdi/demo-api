<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends FOSRestController
{
    /**
     * Category post
     *
     * @Rest\Post("/categories")
     *
     * @param Request $request
     * @return View
     */
    public function postCategories(Request $request): View
    {
        $category = new Category();
        $category->setName($request->get('name'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        return View::create($category, Response::HTTP_OK);
    }


    /**
     * Category list
     *
     * @Rest\Get("/categories")
     *
     * @return View
     */
    public function getCategories(): View
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        /** @var CategoryRepository $categoryRepo */
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepo->list();

        return View::create($categories, Response::HTTP_OK);
    }


    /**
     * Category product list
     *
     * @Rest\Get("/categories/{id}/products/{page}", requirements={"id" = "\d+"}, defaults={"page"=1})
     *
     * @ParamConverter("category", class="App\Entity\Category")
     * @param Category $category
     * @param int $page
     * @return View
     */
    public function listCategoryProducts(Category $category, int $page)
    {
        /** @var ProductRepository $productRepo */
        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepo->findByCategoryAndPage($category, $page);

        return View::create($products, Response::HTTP_OK, array(
            "page_count" => \ceil($category->getProducts()->count() / Product::MAX_COUNT_BY_PAGE)
        ));
    }

    /**
     * data fixtures
     * @Route("/load-data", methods={"GET", "POST"})
     */
    public function dataFixture()
    {
        $em = $this->getDoctrine()->getManager();

        $category = new Category();
        $category->setName("Test Name" . rand(1, 10));
        $em->persist($category);

        for ($i = 0; $i < 20; $i++) {
            $product = new Product();
            $product->setName("Product 1")
                ->setPrice($i + 15.00)
                ->setStock($i + 10);
            $product->addCategory($category);
            $em->persist($product);
        }

        $em->flush();

        return new Response("Data loaded");
    }

}
