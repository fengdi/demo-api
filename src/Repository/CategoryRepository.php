<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * list all categories
     */
    public function list()
    {
        $result = $this->createQueryBuilder('ca')
            ->select('ca.id, ca.name')
            ->addSelect('COUNT(ca.id) AS count_product')
            ->leftJoin('ca.products', 'products')
            ->groupBy('ca.id')
            ->getQuery();

        return $result->getResult();
    }
}
