<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * Find list of products by category
     *
     * @param Category $category
     * @param int $page
     * @return mixed
     */
    public function findByCategoryAndPage(Category $category, int $page = 1)
    {
        $result = $this->createQueryBuilder('p')
            ->select('p.id, p.name, p.price, p.stock')
            ->innerJoin('p.categories', 'ca')
            ->where('ca IN (:category)')
            ->setParameter('category',$category)
            ->orderBy('p.id', 'ASC')
            ->setFirstResult(($page -1) * Product::MAX_COUNT_BY_PAGE)
            ->setMaxResults(Product::MAX_COUNT_BY_PAGE)
            ->getQuery();

        return $result->getResult();
    }
}
